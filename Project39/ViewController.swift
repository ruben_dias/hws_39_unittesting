//
//  ViewController.swift
//  Project39
//
//  Created by Ruben Dias on 21/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    var playData = PlayData()
    var currentFilter: String = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchTapped))
    }

    @objc func searchTapped() {
        let ac = UIAlertController(title: "Filter…", message: nil, preferredStyle: .alert)
        ac.addTextField { [unowned self] in
            if self.currentFilter == "0" {
                $0.text = nil
            } else {
                $0.text = self.currentFilter
            }
        }

        ac.addAction(UIAlertAction(title: "Filter", style: .default) { [unowned self] _ in
            var userInput = ac.textFields?[0].text ?? "0"
            if userInput.isEmpty { userInput = "0" }
            self.currentFilter = userInput
            self.playData.applyUserFilter(userInput)
            self.tableView.reloadData()
        })

        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))

        present(ac, animated: true)
    }
    
}

extension ViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playData.filteredWords.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let word = playData.filteredWords[indexPath.row]
        cell.textLabel!.text = word
        cell.detailTextLabel!.text = "\(playData.wordCounts.count(for: word))"
        
        return cell
    }
    
}
