//
//  Project39Tests.swift
//  Project39Tests
//
//  Created by Ruben Dias on 21/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import XCTest
@testable import Project39

class Project39Tests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testAllWordsLoaded() {
        let playData = PlayData()
        XCTAssertEqual(playData.allWords.count, 18440, "allWords was not 18440")
    }
    
    func testWordCountsAreCorrect() {
        let playData = PlayData()
        XCTAssertEqual(playData.wordCounts.count(for: "home"), 174, "Home does not appear 174 times")
        XCTAssertEqual(playData.wordCounts.count(for: "fun"), 4, "Fun does not appear 4 times")
        XCTAssertEqual(playData.wordCounts.count(for: "mortal"), 41, "Mortal does not appear 41 times")
    }
    
    func testWordsLoadQuickly() {
        measure {
            _ = PlayData()
        }
    }
    
    func testUserFilterWorks() {
        let playData = PlayData()

        playData.applyUserFilter("100")
        XCTAssertEqual(playData.filteredWords.count, 495, "Filtering by words appearing more than 100 times does not find 495 words")

        playData.applyUserFilter("1000")
        XCTAssertEqual(playData.filteredWords.count, 55, "Filtering by words appearing more than 1000 times does not find 55 words")

        playData.applyUserFilter("10000")
        XCTAssertEqual(playData.filteredWords.count, 1, "Filtering by words appearing more than 10000 times does not find 1 word")

        playData.applyUserFilter("test")
        XCTAssertEqual(playData.filteredWords.count, 56, "Filtering by \"test\" does not find 56 matches")

        playData.applyUserFilter("swift")
        XCTAssertEqual(playData.filteredWords.count, 7, "Filtering by \"swift\" does not find 7 matches")

        playData.applyUserFilter("objective-c")
        XCTAssertEqual(playData.filteredWords.count, 0, "Filtering by \"objective-c\" does not find 0 matches")
    }
    
    func testUserFilterRunsQuickly() {
        let playData = PlayData()
        
        measure {
            playData.applyUserFilter("100")
        }
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

}
